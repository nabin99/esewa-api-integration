# eSewa API Integration Project

This Spring Boot application enables users to add products and facilitates the purchase of products using the eSewa payment gateway. For testing purposes, you can use the following credentials:

- **eSewa ID:** 9806800001/2/3/4/5
- **Password:** Nepal@123
- **MPIN:** 1122 (for application use only)
- **Token:** 123456
- **Author:** Nabin Ghatani

## Table of Contents

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Configuration](#configuration)
- [Usage](#usage)
- [Testing](#testing)

## Introduction

This Spring Boot application provides a simple and effective way to integrate with the eSewa payment gateway. It allows users to add products and seamlessly make purchases using the eSewa platform.

## Prerequisites

Before getting started, ensure that you have the following:

- Java Development Kit (JDK)

### Configuration

Configure the eSewa credentials in the application. Modify the `application.properties` as per need.

## Usage

To use the application, follow these steps:

1. Build and run the Spring Boot application.
2. Access the application through the specified endpoints.
3. Add products to the system.
4. Initiate a purchase using the eSewa payment gateway.
5. Test the functionality using the provided eSewa testing credentials.

## Testing

For testing purposes, use the following eSewa credentials:

- **eSewa ID:** 9806800001/2/3/4/5
- **Password:** Nepal@123
- **MPIN:** 1122 (for application use only)
- **Token:** 123456

Ensure that you follow the testing guidelines provided by eSewa for a smooth testing experience.