package com.eSewa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ESewaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ESewaApplication.class, args);
    }


}
