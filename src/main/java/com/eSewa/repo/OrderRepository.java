package com.eSewa.repo;

import com.eSewa.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    Order findByInvoiceId(long invoiceId);
}
