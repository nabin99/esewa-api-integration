package com.eSewa.service;

import com.eSewa.entity.Order;
import com.eSewa.entity.Product;
import com.eSewa.repo.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ProductService productService;

    @Override
    public Order createOrder(int productId) {
        Product product = productService.getProductById(productId);
        Order order = new Order();
        order.setProductId(productId);
        order.setInvoiceId(new Random().nextLong());
        order.setTotal(product.getAmount());
        orderRepository.save(order);
        return order;
    }

    @Override
    public Order getOrderByIdOrderId(int orderId) {
        return orderRepository.findById(orderId).get();
    }

    @Override
    public Order getOrderByInvoiceId(long invoiceId) {
        return orderRepository.findByInvoiceId(invoiceId);
    }

    @Override
    public void saveOrder(Order order) {
        orderRepository.save(order);
    }
}
