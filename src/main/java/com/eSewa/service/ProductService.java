package com.eSewa.service;

import com.eSewa.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    Product getProductById(int productId);
    List<Product> getAllProducts();
    void addProduct(Product product);
}
