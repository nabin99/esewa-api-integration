package com.eSewa.service;

import com.eSewa.entity.Order;
import org.springframework.stereotype.Service;

@Service
public interface OrderService {
    Order createOrder(int productId);
    Order getOrderByIdOrderId(int orderId);
    Order getOrderByInvoiceId(long invoiceId);
    void saveOrder(Order order);
}
