package com.eSewa.controller;

import com.eSewa.entity.Product;
import com.eSewa.service.OrderService;
import com.eSewa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private OrderService orderService;

    @GetMapping("/add-product")
    public ModelAndView getAddProductPage(ModelAndView modelAndView) {
        modelAndView.setViewName("add-product");
        modelAndView.addObject("product", new Product());
        return modelAndView;
    }

    @PostMapping("/add-product")
    public String addProduct(@ModelAttribute Product product) {
        productService.addProduct(product);
        return "redirect:/";
    }

    @GetMapping("/{productId}")
    public ModelAndView getProductDetailsPage(@PathVariable int productId, ModelAndView modelAndView){
        modelAndView.setViewName("product-details");
        modelAndView.addObject("product",productService.getProductById(productId));
        modelAndView.addObject("order",orderService.createOrder(productId));
        return modelAndView;
    }
}
