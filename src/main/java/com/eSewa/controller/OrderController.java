package com.eSewa.controller;

import com.eSewa.entity.Order;
import com.eSewa.model.ApplicationMessage;
import com.eSewa.model.OrderDto;
import com.eSewa.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/{invoiceId}")
    public OrderDto fetchOrderAmountByOrderId(@PathVariable long invoiceId){
        OrderDto orderDto= new OrderDto();
        Order order = orderService.getOrderByInvoiceId(invoiceId);
        if(order!=null)
            orderDto.setTotal(order.getTotal());
        return orderDto;
    }

    @PostMapping("/orderStatus")
    public ApplicationMessage setOrderStatusAfterPayment(@RequestBody OrderDto orderDto) {
        ApplicationMessage applicationMessage = new ApplicationMessage();
        Order order = orderService.getOrderByInvoiceId(orderDto.getInvoiceId());
        if (order != null && orderDto.getStatus()==1) {
            order.setStatus(orderDto.getStatus());
            orderService.saveOrder(order);
            applicationMessage.setSuccess(true);
            applicationMessage.setOrder(order);
            applicationMessage.setMessage("Thank you. You have successfully completed your payment");
        } else {
            applicationMessage.setSuccess(false);
            applicationMessage.setMessage("Changing of parameter value is not allowed !!!");
        }
        return applicationMessage;
    }
}
