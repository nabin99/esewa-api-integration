package com.eSewa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/esewa")
public class EsewaController {

    @GetMapping("/success")
    public String getSuccessPage(){
        return "esewa_payment_success";
    }

    @GetMapping("/failed")
    public String getFailurePage(){
        return "esewa_payment_failed";
    }
}
