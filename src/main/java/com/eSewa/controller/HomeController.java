package com.eSewa.controller;

import com.eSewa.entity.Product;
import com.eSewa.model.ApplicationMessage;
import com.eSewa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {
    @Autowired
    private ProductService productService;

    @GetMapping("")
    public ModelAndView getHomePage(ModelAndView modelAndView){
        modelAndView.setViewName("index");
        modelAndView.addObject("products",productService.getAllProducts());
        return modelAndView;
    }

    @GetMapping("{productId}")
    @ResponseBody
    public ApplicationMessage fetchProductDetailsByProductId(@PathVariable int productId) {
        ApplicationMessage applicationMessage = new ApplicationMessage();
        Product product = productService.getProductById(productId);
        if (product == null) {
            applicationMessage.setSuccess(false);
            applicationMessage.setMessage("Product doesn't found");
        } else {
            applicationMessage.setSuccess(true);
            applicationMessage.setProduct(product);
        }
        return applicationMessage;
    }
}
